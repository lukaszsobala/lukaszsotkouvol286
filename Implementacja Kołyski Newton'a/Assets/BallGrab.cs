﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGrab : MonoBehaviour
{
    Vector3 worldPosition;

    private void GrabBall()
    {
        transform.position = ConvertMousePositionToScreen();
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            GrabBall();
        }
    }

    private Vector3 ConvertMousePositionToScreen()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.nearClipPlane;
        worldPosition = Camera.main.ScreenToWorldPoint(mousePos);

        worldPosition = new Vector3(worldPosition.x, worldPosition.y, 40);

        return worldPosition;
    }
}
