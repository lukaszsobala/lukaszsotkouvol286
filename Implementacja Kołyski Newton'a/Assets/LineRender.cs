﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRender : MonoBehaviour
{
    public float xLineHangPosition = 0;
    public float yLineHangPosition = 0;
    public float xBallPosition = 0;
    public float yBallPosition = 0;

    public LineRenderer lineRenderer;
    public DistanceJoint2D distanceJoin2D;

    private void Start()
    {
        Vector2 hangPosition = new Vector2(xLineHangPosition, yLineHangPosition);

        lineRenderer.SetPosition(0, hangPosition);
        distanceJoin2D.connectedAnchor = hangPosition;

        transform.position = new Vector2(xBallPosition, yBallPosition);
    }

    private void Update()
    {
        lineRenderer.SetPosition(1, transform.position);
    }
}
