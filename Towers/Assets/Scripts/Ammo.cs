﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    [SerializeField] GameObject tower;
    [SerializeField] float minShootDistance = 1;
    [SerializeField] float maxShootDistance = 4;
    [SerializeField] float speed = 4;
    GameObject myTower;
    float shootDistance;
    Vector2 startPosition;

    void Start()
    {
        startPosition = transform.position;
        shootDistance = Random.Range(minShootDistance, maxShootDistance);
    }

    private void Update()
    {
        Travel();

        if (Vector2.Distance(startPosition, transform.position) >= shootDistance)
        {
            InstantiateNewTower();
            Destroy(gameObject);
        }
    }

    private void Travel()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);
    }

    private void InstantiateNewTower()
    {
        var newTower = Instantiate(tower);
        newTower.transform.position = transform.position;
        newTower.transform.rotation = Quaternion.Euler(0, 0, 0);
        StartCoroutine(newTower.GetComponent<RotateTower>().WaitAfterSpawn());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Tower") && collision.gameObject != myTower)
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

    public void SetMyTower(GameObject tower)
    {
        myTower = tower;
    }
}
