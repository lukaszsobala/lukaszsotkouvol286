﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTower : MonoBehaviour
{
    [SerializeField] float minRandomRotation = 15;
    [SerializeField] float maxRandomRotation = 45;
    [SerializeField] float delay = 0.5f;
    [SerializeField] float breakTimeAfterSpawn = 6;

    Shoot shoot;
    public bool startShooting = false;

    private void Start()
    {
        shoot = GetComponent<Shoot>();

        if (startShooting)
        {
            StartCoroutine(RandomRotate());
        }
    }

    private void Update()
    {
        
    }

    private IEnumerator RandomRotate()
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            float randomRotateValue = Random.Range(minRandomRotation, maxRandomRotation);

            transform.rotation = Quaternion.Euler(0, 0, (transform.rotation.eulerAngles.z + randomRotateValue));

            shoot.ShootAmmo();
        }
    }

    public IEnumerator WaitAfterSpawn()
    {
        yield return new WaitForSeconds(breakTimeAfterSpawn);

        // StartCoroutine(RandomRotate()); Not sure why this doesn't work
    }
}
