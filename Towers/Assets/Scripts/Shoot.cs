﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] GameObject Ammo;
    [SerializeField] GameObject Shooter;

    public void ShootAmmo()
    {
        var instantiatedAmmo = Instantiate(Ammo);
        instantiatedAmmo.transform.position = Shooter.transform.position;
        instantiatedAmmo.transform.rotation = transform.rotation;

        instantiatedAmmo.GetComponent<Ammo>().SetMyTower(gameObject);
    }
}
