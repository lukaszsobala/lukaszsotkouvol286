﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerAmount : MonoBehaviour
{
    [SerializeField] GameObject towerAmountTextBox;

    int amountOfTowers = 0;

    public void increaseAmountOfTowers()
    {
        amountOfTowers++;
    }

    public void DecreaseAmountOfTowers()
    {
        amountOfTowers--;
    }

    private void Update()
    {
        towerAmountTextBox.GetComponent<UnityEngine.UI.Text>().text = amountOfTowers.ToString();
    }
}
